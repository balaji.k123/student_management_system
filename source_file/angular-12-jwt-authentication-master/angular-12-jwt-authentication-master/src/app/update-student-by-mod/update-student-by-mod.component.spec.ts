import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStudentByModComponent } from './update-student-by-mod.component';

describe('UpdateStudentByModComponent', () => {
  let component: UpdateStudentByModComponent;
  let fixture: ComponentFixture<UpdateStudentByModComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateStudentByModComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateStudentByModComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
