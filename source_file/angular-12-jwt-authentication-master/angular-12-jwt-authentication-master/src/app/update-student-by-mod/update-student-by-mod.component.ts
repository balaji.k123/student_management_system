import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-update-student-by-mod',
  templateUrl: './update-student-by-mod.component.html',
  styleUrls: ['./update-student-by-mod.component.css'],
})
export class UpdateStudentByModComponent implements OnInit {
  id!: number;
  student: Student_Class = new Student_Class();
  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.userService.getStudentById(this.id).subscribe(
      (data) => {
        this.student = data;
      },
      (error) => console.log(error)
    );
  }
  onSubmit() {
    this.userService.updateStudent(this.id, this.student).subscribe(
      (data) => {
        console.log(data);
        this.studentList();
      },
      (error) => console.log(error)
    );
  }
  studentList() {
    this.router.navigate(['/mod']);
  }
}
