import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { UpdateComponent } from './update/update.component';
import { CreateStudentComponent } from './create-student/create-student.component';
import { StudentalltaskComponent } from './studentalltask/studentalltask.component';
import { AllaccountComponent } from './allaccount/allaccount.component';
import { UpdateStudentComponent } from './update-student/update-student.component';
import { UpdateStudentByModComponent } from './update-student-by-mod/update-student-by-mod.component';
import { StudentallmodComponent } from './studentallmod/studentallmod.component';
import { FooterComponent } from './footer/footer.component';
import { StaffaccountComponent } from './staffaccount/staffaccount.component';
import { StaffUpdateComponent } from './staffaccount/staff-update/staff-update.component';
import { StaffAddComponent } from './staffaccount/staff-add/staff-add.component';
import { ResultPageComponent } from './result-page/result-page.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'update/:id', component: UpdateComponent },
  { path: 'createStudent', component: CreateStudentComponent },
  { path: 'studentAll', component: StudentalltaskComponent },
  { path: 'allAccount', component: AllaccountComponent },
  { path: 'updateStudent/:id', component: UpdateStudentComponent },
  { path: 'updateStudentByMod/:id', component: UpdateStudentByModComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'stafflist', component: StaffaccountComponent },
  { path: 'add-staff', component: StaffAddComponent },
  { path: 'update-staff/:id', component: StaffUpdateComponent },
  { path: 'result', component: ResultPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
