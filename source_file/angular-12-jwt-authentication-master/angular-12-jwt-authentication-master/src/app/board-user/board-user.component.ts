import { Component, OnInit } from '@angular/core';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css'],
})
export class BoardUserComponent implements OnInit {
  student: Student_Class[] = [];
  content?: string;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getStudentAllData().subscribe(
      (data) => {
        this.student = data;
      },
      (err) => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}
