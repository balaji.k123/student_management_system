import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-staff-update',
  templateUrl: './staff-update.component.html',
  styleUrls: ['./staff-update.component.css'],
})
export class StaffUpdateComponent implements OnInit {
  id!: number;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  oldData = new FormGroup({
    staffname: new FormControl(''),
    staffage: new FormControl(''),
    qualifications: new FormControl(''),
    joining_date: new FormControl(''),
    department: new FormControl(''),
    salary: new FormControl(''),
  });

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.userService.getStaffById(this.id).subscribe((data) => {
      console.log(data);

      this.oldData = new FormGroup({
        staffname: new FormControl(data['staffname'], [
          Validators.required,
          Validators.pattern('[a-zA-Z][a-zA-Z ]+'),
          Validators.minLength(3),
        ]),
        staffage: new FormControl(data['staffage'], [
          Validators.required,
          Validators.pattern('^(?:1[8-9]|[3-5][0-9]|60)$'),
        ]),
        qualifications: new FormControl(data['qualifications'], [
          Validators.required,
          Validators.pattern('[a-zA-Z]|[.]&&[a-zA-Z ]+'),
        ]),
        joining_date: new FormControl(
          data['joining_date'],
          Validators.required
        ),
        department: new FormControl(data['department'], [
          Validators.required,
          Validators.pattern('[a-zA-Z][a-zA-Z ]+'),
        ]),
        salary: new FormControl(data['salary'], [
          Validators.required,
          Validators.min(15000),
        ]),
      });
    });
  }

  saveData() {
    this.userService
      .updateStaffById(this.id, this.oldData.value)
      .subscribe((data) => {
        console.log(data);
        this.router.navigate(['stafflist']);
      });
  }
}
