import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-staff-add',
  templateUrl: './staff-add.component.html',
  styleUrls: ['./staff-add.component.css'],
})
export class StaffAddComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}

  minNum = 15000;

  staffadd = new FormGroup({
    staffname: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z][a-zA-Z ]+'),
      Validators.minLength(3),
      Validators.maxLength(15),
    ]),
    staffage: new FormControl('', [
      Validators.required,
      Validators.pattern('^(?:1[8-9]|[3-5][0-9]|60)$'),
    ]),
    qualifications: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z]|[.]&&[a-zA-Z ]+'),
    ]),
    joining_date: new FormControl('', Validators.required),
    department: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z][a-zA-Z ]+'),
    ]),
    salary: new FormControl('', [
      Validators.required,
      Validators.min(this.minNum),
    ]),
  });

  ngOnInit(): void {}

  saveData() {
    this.userService.addStaff(this.staffadd.value).subscribe(
      (Data) => {
        console.log(Data);
        this.router.navigate(['/stafflist']);
      },
      (error) => console.log(error)
    );
  }
}
