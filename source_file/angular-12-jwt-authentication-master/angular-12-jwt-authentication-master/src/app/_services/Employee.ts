export class Employee {
  id!: number;
  username!: String;
  email!: String;
  password!: String;
  roles!: any;
}
