import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllaccountComponent } from './allaccount.component';

describe('AllaccountComponent', () => {
  let component: AllaccountComponent;
  let fixture: ComponentFixture<AllaccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllaccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
