import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../_services/Employee';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-allaccount',
  templateUrl: './allaccount.component.html',
  styleUrls: ['./allaccount.component.css'],
})
export class AllaccountComponent implements OnInit {
  employee: Employee[] = [];
  constructor(private userservice: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getemployeeslist();
  }
  private getemployeeslist() {
    this.userservice.getAdminBoard().subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].roles[0].id == 2) {
          this.employee.push(data[i]);
        }
      }

      console.log(data);
    });
  }
  updateById(id: number) {
    this.router.navigate(['/update', id]);
  }
  deleteById(id: number) {
    this.userservice.deleteById(id).subscribe(() => {
      this.getemployeeslist();
    });
  }
}
