import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentallmodComponent } from './studentallmod.component';

describe('StudentallmodComponent', () => {
  let component: StudentallmodComponent;
  let fixture: ComponentFixture<StudentallmodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentallmodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentallmodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
