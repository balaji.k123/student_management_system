import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css'],
})
export class CreateStudentComponent implements OnInit {
  student: Student_Class = new Student_Class();

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    // getStudentRecordAll() {
    //     this.router.navigate(['/mod']);
    //   }
  }

  createStudent() {
    this.userService.createStudent(this.student).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => console.log(error)
    );
    // this.getStudentRecordAll();
    this.ngOnInit();
    this.router.navigate(['/studentAll']);
  }
}
