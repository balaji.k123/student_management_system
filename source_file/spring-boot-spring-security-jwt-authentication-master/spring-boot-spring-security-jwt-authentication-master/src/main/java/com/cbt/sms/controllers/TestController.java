package com.cbt.sms.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cbt.sms.Exception.ResourceNotFoundException;
import com.cbt.sms.models.StudentDetails;
import com.cbt.sms.models.User;
import com.cbt.sms.repository.StudentRepository;
import com.cbt.sms.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private UserRepository userRepo;

	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}

//	public List<User> getUser() {
//		return this.userRepo.findAll();
//	}

//	@GetMapping("/user")
//	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
//	public String userAccess() {
//		return "User Content.";
//	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

//get Panna
	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public List<User> getUser() {
		return this.userRepo.findAll();
	}

	@GetMapping("/admin/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<User> getUserById(@PathVariable Long id) {
		User user = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		return ResponseEntity.ok(user);
	}

	@PutMapping("/admin/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody User updateUser) {
		User user = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		user.setUsername(updateUser.getUsername());
		user.setEmail(updateUser.getEmail());
		user.setPassword(updateUser.getPassword());
		User updateEmployee = userRepo.save(user);
		return ResponseEntity.ok(updateEmployee);
	}

	@DeleteMapping("/admin/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Map<String, Boolean>> userDelete(@PathVariable Long id) {
		User user = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		userRepo.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}

//////////////////////////////////////////////////////////////
	///////////// student ///////////////////
	/////////////////Create new Student////////
	@PostMapping("/admin/createStudent")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public StudentDetails createStudent(@RequestBody StudentDetails studentDetails) {
		return studentRepository.save(studentDetails);
	}
/////////////get all Student//////
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')or hasRole('USER')")
	public List<StudentDetails> getStudent() {
		return this.studentRepository.findAll();
	}

	////////student get by id
	@GetMapping("/admin/student/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public ResponseEntity<StudentDetails> getStudentById(@PathVariable Long id) {
		StudentDetails user = studentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		return ResponseEntity.ok(user);
	}
	//////////////////////student update record ////////////
	@PutMapping("/admin/student/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public ResponseEntity<StudentDetails> updateStudent(@PathVariable Long id, @RequestBody StudentDetails studentDetails) {
		StudentDetails user = studentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		user.setS_name(studentDetails.getS_name());
		user.setS_age(studentDetails.getS_age());
		user.setS_name(studentDetails.getS_name());
		user.setS_class(studentDetails.getS_class());
		user.setS_dob(studentDetails.getS_dob());
		user.setS_phoneNumber(studentDetails.getS_phoneNumber());
		user.setS_Address(studentDetails.getS_Address());
		StudentDetails updateEmployee = studentRepository.save(user);
		return ResponseEntity.ok(updateEmployee);
	}
	
/////////student delete by id
	@DeleteMapping("/admin/student/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Map<String, Boolean>> studentDelete(@PathVariable Long id) {
		StudentDetails user = studentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user id not found" + id + "no record"));
		studentRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}

}
