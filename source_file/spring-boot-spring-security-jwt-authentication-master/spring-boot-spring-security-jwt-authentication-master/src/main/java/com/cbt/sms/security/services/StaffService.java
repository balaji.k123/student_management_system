package com.cbt.sms.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cbt.sms.Exception.ResourceNotFoundException;
import com.cbt.sms.models.Staff;
import com.cbt.sms.repository.StaffRepository;

@Service
public class StaffService {
	
	@Autowired
	private StaffRepository staffRep;
	
	public List<Staff> getlist(){
		
		return this.staffRep.findAll();
	}
	
	public Staff addData(Staff Data){
		
		return this.staffRep.save(Data);
	}
	
	public Optional<Staff> getByid(Long id){
		
		return this.staffRep.findById(id);
	}
	
	public Staff update(Long id,Staff data) {

		Staff oldData = this.staffRep.findById(id)
				.orElseThrow(()->new ResourceNotFoundException("Note:Id is invalid.... Can't update "));
		oldData.setStaffname(data.getStaffname());
		oldData.setStaffage(data.getStaffage());
		oldData.setQualifications(data.getQualifications());
		oldData.setJoining_date(data.getJoining_date());
		oldData.setDepartment(data.getDepartment());
		oldData.setSalary(data.getSalary());
		
		return this.staffRep.save(oldData);
	}
	
	public void delete(Long id) {
		
		this.staffRep.deleteById(id);
	}

}
