package com.cbt.sms.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cbt.sms.models.Staff;
import com.cbt.sms.security.services.StaffService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class StaffController {
	
	@Autowired
	private StaffService staffService;
	
	//get staff list
			@GetMapping("Stafflist")
			@PreAuthorize("hasRole('ADMIN')")
			public List<Staff> getStafflist(){
				return this.staffService.getlist();
			}
			
			//Add staff
			@PostMapping("/addstaff")
			@PreAuthorize("hasRole('ADMIN')")
			public Staff AddStaff(@RequestBody Staff data) {
				return this.staffService.addData(data);
			}
			
			//get Staff by id
			@GetMapping("getStaffById/{id}")
			@PreAuthorize("hasRole('ADMIN')")
			public Optional<Staff>getById(@PathVariable(value = "id") Long id){
				return this.staffService.getByid(id);
			}
			
			//update staff
			@PutMapping("/updateStaffByid/{id}")
			@PreAuthorize("hasRole('ADMIN')")
			public Staff updateStaff(@PathVariable(value="id")Long id,
					@RequestBody Staff data) {
				return this.staffService.update(id, data);
			}
			
			//delete Staff
			@DeleteMapping("deleteStaff/{id}")
			@PreAuthorize("hasRole('ADMIN')")
			public void deleteStaff(@PathVariable(value="id") Long id) {
				this.staffService.delete(id);
			}



}
