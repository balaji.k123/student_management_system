package com.cbt.sms.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class StudentDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="studentId")
	private Long id;
	private String s_name;
	private Long s_age;
	private String s_class;
	private String s_dob;
	private Long s_phoneNumber;
	private String s_Address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getS_name() {
		return s_name;
	}

	public void setS_name(String s_name) {
		this.s_name = s_name;
	}

	public Long getS_age() {
		return s_age;
	}

	public void setS_age(Long s_age) {
		this.s_age = s_age;
	}

	public String getS_class() {
		return s_class;
	}

	public void setS_class(String s_class) {
		this.s_class = s_class;
	}

	public String getS_dob() {
		return s_dob;
	}

	public void setS_dob(String s_dob) {
		this.s_dob = s_dob;
	}

	public Long getS_phoneNumber() {
		return s_phoneNumber;
	}

	public void setS_phoneNumber(Long s_phoneNumber) {
		this.s_phoneNumber = s_phoneNumber;
	}

	public String getS_Address() {
		return s_Address;
	}

	public void setS_Address(String s_Address) {
		this.s_Address = s_Address;
	}

	public StudentDetails(Long id, String s_name, String role_number, Long s_age, String s_class, String s_dob,
			Long s_phoneNumber, String s_Address) {
		super();
		this.id = id;
		this.s_name = s_name;
		this.s_age = s_age;
		this.s_class = s_class;
		this.s_dob = s_dob;
		this.s_phoneNumber = s_phoneNumber;
		this.s_Address = s_Address;
	}

	public StudentDetails() {
		super();
	}

}
