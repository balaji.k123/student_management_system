package com.cbt.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cbt.sms.models.StudentDetails;

public interface StudentRepository extends JpaRepository<StudentDetails, Long> {

}
